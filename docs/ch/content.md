# FEALPy: Finite Element Analysis Library in Python 

FEALPy 是一款偏微分方程数值解的开源算法库，以对象化和数组化的编程技术实现，简单
易用且易维护扩展，已集成了丰富的网格数据结构和数值离散方法，主要用于支撑相关领域
的基础算法研究与人才培养，也可用于CAE原型应用软件的快速开发验证。


* [简介](./introduction.md)
* [网格及其数据结构](./mesh.md)
* [偏微分方程数值离散方法](./num_methods.md)
    + [有限元方法](./fem.md)
    + [有限体积方法](./fvm.md)
    + [有限差分方法](./fdm.md)
    + [虚单元方法](./vem.md)
    + [弱 Galerkin 方法](./wgm.md)
* [数值算例](./examples.md)
    + [Poisson 问题](./poisson.md)
    + [非线性 Poisson 问题](./non_linear_poisson.md)
    + [一般的椭圆方程](./general_elliptic_equation.md)
* [贡献](./contribution.md)
